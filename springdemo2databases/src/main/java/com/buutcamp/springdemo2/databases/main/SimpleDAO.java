package com.buutcamp.springdemo2.databases.main;

import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.List;

public class SimpleDAO {

    private JdbcTemplate jdbcTemplate;

    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List<String> getData() {
        String sqlQuery = "SELECT * FROM buutcamp_table";
        List<String> tmplist = jdbcTemplate.query(sqlQuery,new SimpleRowMapper());

        return tmplist;
    }


}
