package com.buutcamp.springdemo2.databases.main;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SimpleRowMapper implements RowMapper<String>{

    public String mapRow(ResultSet rs, int rowNum) throws SQLException {
        String tmp;
        tmp = rs.getString("col_1");

        return tmp;
    }
}
