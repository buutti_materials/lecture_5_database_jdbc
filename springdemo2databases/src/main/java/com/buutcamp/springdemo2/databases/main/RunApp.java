package com.buutcamp.springdemo2.databases.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

public class RunApp {

    public RunApp() {
        //Create business logic here
        ApplicationContext appCtx =
                new ClassPathXmlApplicationContext("beans-cp.xml");

        SimpleDAO dao = (SimpleDAO) appCtx.getBean("simpleDAO");

        List<String> tmp = dao.getData();

        System.out.println(tmp.get(0));
        System.out.println(tmp.get(1));

        ((ClassPathXmlApplicationContext) appCtx).close();

    }
}
